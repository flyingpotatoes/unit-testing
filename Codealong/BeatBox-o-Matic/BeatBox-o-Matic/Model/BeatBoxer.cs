﻿using System;
using System.IO;

namespace BeatBox_o_Matic.Model
{
    public class BeatBoxer
    {
        public void LayDownASickBeat(int durationOfSickBeat, string filePath)
        {
            string[] words = {"Boots", "and", "Kitties", "and"}; //man we're cool
            const int wordsPerLine = 15;

            var sickBeat = "";

            if (File.Exists(filePath)) 
            {
                File.Delete(filePath); //don't want our sick beats stacking up forever
            }

            for (var i = 0; i < durationOfSickBeat; i++)
            {
                sickBeat += words[i % 4] + " "; //incrementally add words to our sick beat, going back to beginning when we get to the end

                if (i % wordsPerLine == wordsPerLine - 1) //every X words, jump to the next line to avoid beat overload
                {
                    sickBeat += Environment.NewLine;
                }
            }

            File.AppendAllText(filePath, sickBeat);
        }
    }
}
