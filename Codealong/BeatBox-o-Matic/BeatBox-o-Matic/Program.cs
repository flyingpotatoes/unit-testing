﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeatBox_o_Matic.Model;

namespace BeatBox_o_Matic
{
    class Program
    {
        static void Main(string[] args)
        {
            var beatBoxer = new BeatBoxer();

            beatBoxer.LayDownASickBeat(3000, "beatboxoutput.txt");
        }
    }
}
