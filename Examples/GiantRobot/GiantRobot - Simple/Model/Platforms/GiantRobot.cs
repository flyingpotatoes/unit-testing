﻿namespace GiantRobot___Simple.Model.Platforms
{
    public class GiantRobot
    {
        public virtual string MakeNoises()
        {
            return "bleep bloop";
        }

        public virtual string StompOnStuff()
        {
            return "OH NO THE GIANT ROBOT IS STOMPING ON ALL MY STUFF";
        }
    }
}
