﻿using System.Diagnostics;
using GiantRobot___Simple.Model.Platforms;

namespace GiantRobot___Simple
{
    class Program
    {
        static void Main(string[] args)
        {
            var giantRobot = new GiantRobot();

            Debug.Print(giantRobot.MakeNoises());
            Debug.Print(giantRobot.StompOnStuff());
        }
    }
}
