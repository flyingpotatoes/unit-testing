﻿using System.Collections.Generic;
using GiantRobot___DependencyInjected.Model.Limbs;
using GiantRobot___DependencyInjected.Model.Platforms;
using Moq;
using NUnit.Framework;

namespace GiantRobot___DependencyInjectedTests.Model.Platforms
{
    [TestFixture]
    public class OctopusRobotTests
    {
        private OctopusRobot _octopusRobot;

        private Mock<IRobotLimb> _limbA;
        private const string LimbAOutput = "Arm did something";
        
        private Mock<IRobotLimb> _limbB;
        private const string LimbBOutput = "Arm did something else";

        private Mock<IRobotLimb> _limbC;
        private const string LimbCOutput = "Arm did yet another thing";

        [SetUp]
        public void SetUp()
        {
            _limbA = new Mock<IRobotLimb>();
            _limbA.Setup(x => x.Use()).Returns(LimbAOutput);

            _limbB = new Mock<IRobotLimb>();
            _limbB.Setup(x => x.Use()).Returns(LimbBOutput);

            _limbC = new Mock<IRobotLimb>();
            _limbC.Setup(x => x.Use()).Returns(LimbCOutput);
        }

        [Test]
        public void MakeNoises_GoesBlubBlub()
        {
            _octopusRobot = new OctopusRobot(null);

            Assert.AreEqual("Blub blub", _octopusRobot.MakeNoises());
        }

        [Test]
        public void UseLimbs_ReturnsNull_WhenNoLimbsSpecified()
        {
            _octopusRobot = new OctopusRobot(null);

            Assert.IsNull(_octopusRobot.UseLimbs());
        }

        [Test]
        public void UseLimbs_ReturnsLimbOutput_WhenOnlyOneLimb()
        {
            _octopusRobot = new OctopusRobot(new List<IRobotLimb> { _limbA.Object });

            Assert.AreEqual(LimbAOutput, _octopusRobot.UseLimbs());

            _limbA.Verify(x => x.Use(), Times.Once);
        }

        [Test]
        public void UseLimbs_ReturnsConcatenatedLimbOutput_WhenMultipleLimbs()
        {
            _octopusRobot = new OctopusRobot(new List<IRobotLimb> { _limbA.Object, _limbB.Object, _limbC.Object });
            var expectedOutput = $"{LimbAOutput}, {LimbBOutput}, {LimbCOutput}"; //looks like: "Arm did something, Arm did something else, Arm did yet another thing"

            Assert.AreEqual(expectedOutput, _octopusRobot.UseLimbs());

            _limbA.Verify(x => x.Use(), Times.Once);
            _limbB.Verify(x => x.Use(), Times.Once);
            _limbC.Verify(x => x.Use(), Times.Once);
        }
    }
}
