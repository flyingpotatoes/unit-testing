﻿using GiantRobot___DependencyInjected.Model.Platforms;
using NUnit.Framework;

namespace GiantRobot___DependencyInjectedTests.Model.Platforms
{
    [TestFixture]
    public class GiantRobotWithLaserCannonArmTests
    {
        private GiantRobotWithLaserCannonArm _robot;

        [SetUp]
        public void SetUp()
        {
            _robot = new GiantRobotWithLaserCannonArm();
        }

        [Test]
        public void UseArm_FiresLaser()
        {
            Assert.AreEqual("PEW PEW", _robot.UseArm());
        }
    }
}
