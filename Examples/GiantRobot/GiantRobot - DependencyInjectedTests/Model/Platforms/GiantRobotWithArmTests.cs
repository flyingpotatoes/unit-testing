﻿using GiantRobot___DependencyInjected.Model.Limbs;
using GiantRobot___DependencyInjected.Model.Platforms;
using Moq;
using NUnit.Framework;

namespace GiantRobot___DependencyInjectedTests.Model.Platforms
{
    [TestFixture]
    public class GiantRobotWithArmTests
    {
        private GiantRobotWithArm _robot;
        private Mock<IRobotLimb> _arm;

        private const string ArmOutput = "Arm did something";

        [SetUp]
        public void SetUp()
        {
            _arm = new Mock<IRobotLimb>();
            _arm.Setup(x => x.Use()).Returns(ArmOutput);

            _robot = new GiantRobotWithArm(_arm.Object);
        }

        [Test]
        public void UseArm_ReturnsNull_WhenNoArm()
        {
            _robot = new GiantRobotWithArm(null);

            Assert.IsNull(_robot.UseArm());
        }


        [Test]
        public void UseArm_ReturnsArmOutput_WhenArmPresent()
        {
            Assert.AreEqual(ArmOutput, _robot.UseArm());

            _arm.Verify(x => x.Use(), Times.Once);
        }
    }
}
