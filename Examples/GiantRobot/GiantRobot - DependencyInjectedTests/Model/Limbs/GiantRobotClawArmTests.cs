﻿using GiantRobot___DependencyInjected.Model.Limbs;
using NUnit.Framework;

namespace GiantRobot___DependencyInjectedTests.Model.Limbs
{
    [TestFixture]
    public class GiantRobotClawArmTests
    {
        private GiantRoboticClawArm _arm;

        [SetUp]
        public void SetUp()
        {
            _arm = new GiantRoboticClawArm();
        }

        [Test]
        public void Use_DoesClawStuff()
        {
            Assert.AreEqual("BEWARE OF THE CLAW", _arm.Use());
        }
    }
}
