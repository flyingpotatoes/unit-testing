﻿using GiantRobot___DependencyInjected.Model.Limbs;
using NUnit.Framework;

namespace GiantRobot___DependencyInjectedTests.Model.Limbs
{
    [TestFixture]
    public class LaserCannonArmTests
    {
        private LaserCannonArm _arm;

        [SetUp]
        public void SetUp()
        {
            _arm = new LaserCannonArm();
        }

        [Test]
        public void Use_DoesLaserStuff()
        {
            Assert.AreEqual("PEW PEW", _arm.Use());
        }
    }
}
