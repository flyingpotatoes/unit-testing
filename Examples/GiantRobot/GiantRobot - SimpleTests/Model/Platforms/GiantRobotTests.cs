﻿using GiantRobot___Simple.Model.Platforms;
using NUnit.Framework;

namespace GiantRobot___SimpleTests.Model.Platforms
{
    [TestFixture]
    public class GiantRobotTests
    {
        private GiantRobot _giantRobot;

        [SetUp]
        public void SetUp()
        {
            _giantRobot = new GiantRobot();
        }

        [Test]
        public void MakeNoises_MakesABleepBloopNoise()
        {
            Assert.AreEqual("bleep bloop", _giantRobot.MakeNoises());
        }

        [Test]
        public void StompOnStuff_StompsOnAllMyStuff()
        {
            Assert.AreEqual("OH NO THE GIANT ROBOT IS STOMPING ON ALL MY STUFF", _giantRobot.StompOnStuff());
        }
    }
}
