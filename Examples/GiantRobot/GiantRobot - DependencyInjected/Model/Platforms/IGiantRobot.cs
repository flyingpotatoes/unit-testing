﻿namespace GiantRobot___DependencyInjected.Model.Platforms
{
    public interface IGiantRobot
    {
        string StompOnStuff();
        string MakeNoises();
    }
}
