﻿namespace GiantRobot___DependencyInjected.Model.Platforms
{
    public interface IGiantRobotWithArm : IGiantRobot
    {
        string UseArm();
    }
}