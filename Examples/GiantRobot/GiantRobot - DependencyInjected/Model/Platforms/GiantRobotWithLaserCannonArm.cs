﻿using GiantRobot___DependencyInjected.Model.Limbs;
using GiantRobot___Simple.Model.Platforms;

namespace GiantRobot___DependencyInjected.Model.Platforms
{
    public class GiantRobotWithLaserCannonArm : GiantRobot
    {
        private readonly LaserCannonArm _arm;

        public GiantRobotWithLaserCannonArm()
        {
            _arm = new LaserCannonArm();
        }

        public string UseArm()
        {
            return _arm.Use();
        }
    }
}
