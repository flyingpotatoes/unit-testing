﻿namespace GiantRobot___DependencyInjected.Model.Platforms
{
    public interface IOctopusRobot : IGiantRobot
    {
        string UseLimbs();
    }
}