﻿using GiantRobot___DependencyInjected.Model.Limbs;
using GiantRobot___Simple.Model.Platforms;

namespace GiantRobot___DependencyInjected.Model.Platforms
{
    public class GiantRobotWithArm : GiantRobot, IGiantRobotWithArm
    {
        private readonly IRobotLimb _arm;

        public GiantRobotWithArm(IRobotLimb arm)
        {
            _arm = arm;
        }

        public string UseArm()
        {
            if (_arm == null)
                return null;

            return _arm.Use();
        }
    }
}
