﻿using System.Collections.Generic;
using System.Linq;
using GiantRobot___DependencyInjected.Model.Limbs;
using GiantRobot___Simple.Model.Platforms;

namespace GiantRobot___DependencyInjected.Model.Platforms
{
    public class OctopusRobot : GiantRobot, IOctopusRobot
    {
        private readonly IEnumerable<IRobotLimb> _limbs;

        public OctopusRobot(IEnumerable<IRobotLimb> limbs)
        {
            _limbs = limbs;
        }

        public override string MakeNoises()
        {
            return "Blub blub";
        }

        public string UseLimbs()
        {
            return _limbs == null 
                ? null 
                : string.Join(", ", _limbs.Select(x => x.Use()));
        }
    }
}
