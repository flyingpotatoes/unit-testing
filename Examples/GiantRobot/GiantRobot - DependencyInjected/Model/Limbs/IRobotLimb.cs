﻿
namespace GiantRobot___DependencyInjected.Model.Limbs
{
    public interface IRobotLimb
    {
        string Use();
    }
}
