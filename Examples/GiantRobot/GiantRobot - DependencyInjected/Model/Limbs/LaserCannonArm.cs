﻿
namespace GiantRobot___DependencyInjected.Model.Limbs
{
    public class LaserCannonArm : IRobotLimb
    {
        public string Use()
        {
            return "PEW PEW";
        }
    }
}
