﻿using System.Collections.Generic;
using GiantRobot___DependencyInjected.Model.Limbs;
using GiantRobot___DependencyInjected.Model.Platforms;
using StructureMap;

namespace GiantRobot___DependencyInjected.IOC
{
    public class IocContainer
    {
        private static Container _container;
        public static Container Container
        {
            get
            {
                return _container ?? (_container = new Container(x =>
                {
                    x.For<IGiantRobotWithArm>().Use<GiantRobotWithArm>()
                        .Ctor<IRobotLimb>().Is<LaserCannonArm>();

                    x.For<IOctopusRobot>().Use<OctopusRobot>()
                        .Ctor<IEnumerable<IRobotLimb>>().Is(y => new List<IRobotLimb>
                        {
                            y.GetInstance<LaserCannonArm>(),
                            y.GetInstance<GiantRoboticClawArm>()
                        });
                }));
            }
        }
    }
}
