﻿using System.Diagnostics;
using GiantRobot___DependencyInjected.IOC;
using GiantRobot___DependencyInjected.Model.Platforms;

namespace GiantRobot___DependencyInjected
{
    class Program
    {
        static void Main(string[] args)
        {
            //hardcoded robot
            var giantRobotWithLaserCannonArm = new GiantRobotWithLaserCannonArm();
            Debug.Print(giantRobotWithLaserCannonArm.MakeNoises());
            Debug.Print(giantRobotWithLaserCannonArm.StompOnStuff());
            Debug.Print(giantRobotWithLaserCannonArm.UseArm());

            //DI robot
            var diRobot = IocContainer.Container.GetInstance<IGiantRobotWithArm>();
            Debug.Print(diRobot.MakeNoises());
            Debug.Print(diRobot.StompOnStuff());
            Debug.Print(diRobot.UseArm());

            //DI octopus robot
            var diOctopusRobot = IocContainer.Container.GetInstance<IOctopusRobot>();
            Debug.Print(diOctopusRobot.MakeNoises());
            Debug.Print(diOctopusRobot.StompOnStuff());
            Debug.Print(diOctopusRobot.UseLimbs());
        }
    }
}
