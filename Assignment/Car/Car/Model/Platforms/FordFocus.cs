﻿using Car.Model.Components;

namespace Car.Model.Platforms
{
    public class FordFocus : ICar
    {
        private readonly INoiseEmitter _noiseEmitter;
        private readonly IEngine _engine;
        private readonly ISteeringWheel _steeringWheel;

        private const int ThrottleIncrement = 10;

        public FordFocus(IEngine engine, ISteeringWheel steeringWheel, INoiseEmitter noiseEmitter)
        {
            _engine = engine;
            _steeringWheel = steeringWheel;
            _noiseEmitter = noiseEmitter;
        }

        public void Start()
        {
            _engine.Start();
        }

        public void Stop()
        {
            _engine.Stop();
        }

        public void SpeedUp()
        {
            if (_engine.Throttle + ThrottleIncrement > _engine.MaxThrottle)
                _engine.Throttle = _engine.MaxThrottle;
            else
                _engine.Throttle += ThrottleIncrement;

            _noiseEmitter.MakeSound($"Engine RPM at {_engine.Rpm}");
        }

        public void SlowDown()
        {
            if (_engine.Throttle - ThrottleIncrement < _engine.MinThrottle)
                _engine.Throttle = _engine.MinThrottle;
            else
                _engine.Throttle -= ThrottleIncrement;

            _noiseEmitter.MakeSound($"Engine RPM at {_engine.Rpm}");
        }

        public void Turn(Direction direction)
        {
            SlowDown();
            _steeringWheel.Turn(direction);
            SpeedUp();
        }
    }
}
