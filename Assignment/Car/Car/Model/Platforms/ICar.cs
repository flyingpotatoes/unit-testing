﻿namespace Car.Model.Platforms
{
    public interface ICar
    {
        void Start();
        void Stop();
        void SpeedUp();
        void SlowDown();
        void Turn(Direction direction);
    }
}