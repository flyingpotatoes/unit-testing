﻿namespace Car.Model.Components
{
    public interface INoiseEmitter
    {
        void MakeSound(string sound);
    }
}