﻿using System;

namespace Car.Model.Components
{
    public class Engine : IEngine
    {
        private readonly INoiseEmitter _noiseEmitter;

        private const int RpmConversionFactor = 100;
        private const int IdleRpm = 700;
        public int MinThrottle => 0;
        public int MaxThrottle => 100;

        private int _throttle;
        public int Throttle
        {
            get => _throttle;
            set
            {
                if (value < MinThrottle)
                    throw new ArgumentOutOfRangeException($"Throttle cannot be below {MinThrottle}");

                if (value > MaxThrottle)
                    throw new ArgumentOutOfRangeException($"Throttle cannot exceed {MaxThrottle}");

                if (IsStarted)
                {
                    if (value > _throttle)
                        _noiseEmitter.MakeSound("vroooooOOOOOOOMMM");

                    if (value < _throttle)
                        _noiseEmitter.MakeSound("VROOOOooooommm");
                }

                _throttle = value;
            }
        }

        public int Rpm => (Throttle * RpmConversionFactor) + IdleRpm;

        public bool IsStarted { get; private set; }

        public Engine(INoiseEmitter noiseEmitter)
        {
            _noiseEmitter = noiseEmitter;
            _throttle = 0;
        }


        public void Start()
        {
            IsStarted = true;
            Throttle = 0;
            _noiseEmitter.MakeSound("ErrrRrRrRVROOOOM");
        }

        public void Stop()
        {
            IsStarted = false;
            Throttle = 0;
            _noiseEmitter.MakeSound("VROOOOooomm...*sputter*..*cough*");
        }
    }
}
