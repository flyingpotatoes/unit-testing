﻿namespace Car.Model.Components
{
    public interface IEngine
    {
        int Throttle { get; set; }
        int Rpm { get; }
        bool IsStarted { get; }
        void Start();
        void Stop();
        int MinThrottle { get; }
        int MaxThrottle {get; }
    }
}