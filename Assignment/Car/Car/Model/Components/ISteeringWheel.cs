﻿namespace Car.Model.Components
{
    public interface ISteeringWheel
    {
        Direction Direction { get; }
        void Turn(Direction direction);
    }
}