﻿using System.Diagnostics;

namespace Car.Model.Components
{
    public class DebugNoiseEmitter : INoiseEmitter
    {
        public void MakeSound(string sound)
        {
            Debug.Print(sound);
        }
    }
}
