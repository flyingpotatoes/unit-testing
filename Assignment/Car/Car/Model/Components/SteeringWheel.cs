﻿
namespace Car.Model.Components
{
    public class SteeringWheel : ISteeringWheel
    {
        private readonly INoiseEmitter _noiseEmitter;

        public Direction Direction { get; private set; }

        public SteeringWheel(INoiseEmitter noiseEmitter)
        {
            _noiseEmitter = noiseEmitter;
            Direction = Direction.Straight;
        }


        public void Turn(Direction direction)
        {
            Direction = direction;
            _noiseEmitter.MakeSound($"Steering wheels don't make noise but this one just turned {direction}");
        }
    }
}
