﻿namespace Car.Model
{
    public enum Direction
    {
        Left,
        Right,
        Straight
    }
}
