﻿using Car.Model.Components;
using Car.Model.Platforms;
using StructureMap;

namespace Car.IoC
{
    public class IocContainer
    {
        private static Container _container;
        public static Container Container
        {
            get
            {
                return _container ?? (_container = new Container(x =>
                {
                    x.For<ICar>().Use<FordFocus>();
                    x.For<ISteeringWheel>().Use<SteeringWheel>();
                    x.For<IEngine>().Use<Engine>();
                    x.For<INoiseEmitter>().Use<DebugNoiseEmitter>();
                }));
            }
        }
    }
}
