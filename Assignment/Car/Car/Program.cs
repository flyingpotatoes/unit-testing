﻿using Car.IoC;
using Car.Model;
using Car.Model.Platforms;

namespace Car
{
    class Program
    {
        static void Main(string[] args)
        {
            var car = IocContainer.Container.GetInstance<ICar>();
         
            car.Start();
            car.SpeedUp();
            car.SpeedUp();
            car.SlowDown();
            car.Turn(Direction.Left);
            car.SpeedUp();
            car.Turn(Direction.Right);
            car.SlowDown();
            car.Stop();
        }
    }
}
